Drupal.behaviors.featuresOverviewToggleColumns = {
  attach: function(context, settings) {
    var $ = jQuery;
    $('a.column-toggle').click(function() {
      var toggle_feature =  $(this).attr("data-toggle-feature");
      var target = '.feature-' + toggle_feature;
      var $target = $(target);
      $target.find('a.column-toggle').toggleClass('toggle-inactive');
      $target.toggleClass('column-hidden').find('div.cell-data').toggle();
      return false;
    });
  }
};