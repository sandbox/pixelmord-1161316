Features overview
------------

This module is a utility that provides an overview page of the custom features created with 
the Features module. It shows the configuration that is exported into the various features 
in structured tables, helping the developers to get a quick overview on which configuration 
is exported into which feature.

For every section of the exported configuration there is a table, 
dependencies of features and other modules are shown in a matrix style table.

### Requirements

- Drupal 7
- Features (see http://www.drupal.org/project/features) 

### Roadmap

- better user interface for the display of really many features
- JQuery functionality to toggle table columns
- tree view showing a hierarchy of features depending on other features, 
  to quickly view "circular" dependencies
  
Installation
------------
Features overview can be installed like any other Drupal module -- place it in the
modules directory for your site and enable it on the `admin/build/modules` page.

Basic usage
-----------
Features overview provides a page that can be found on 'admin/structure/features/overview'.
The various sections of exported configuration are shown in tables, sorted by the website's features.
Feature names are linked to the corresponding detail page provided by the features module.

Maintainer
-----------
- pixelmord (Andreas Sahle)