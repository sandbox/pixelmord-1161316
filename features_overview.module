<?php
/*
 *
 * @file
 * A module to show an overview of the configuration exported to features
 *
 */

/*
 * Implements hook_menu()
 */
function features_overview_menu() {
  $items['admin/structure/features/overview'] = array(
    'title' => 'Overview',
    'type' => MENU_LOCAL_TASK,
    'access arguments' => array('manage features'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('features_overview_page'),
  );
  return $items;
}
/*
 * Page callback function.
 */
function features_overview_page($form, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'features_overview') . '/css/features_overview.css');
  drupal_add_js(drupal_get_path('module', 'features_overview') . '/js/features_overview.js');
  // Load features admin functions.
  module_load_include('inc', 'features', 'features.admin');
  // Load export functions to use in comparison.
  module_load_include('inc', 'features', 'features.export');

  // Clear & rebuild key caches.
  features_get_info(NULL, NULL, TRUE);
  features_rebuild();

  $features = array_filter(features_get_features(), 'features_filter_hidden');
  // write infos about the feature modules in a structured array.
  $feature_infos = _features_overview_get_infos($features);

  //build the checkboxes to let user select, which feature modules to show.
  if(!empty($feature_infos['modules'])) {
    
    $form['feature_select_fieldset'] = array(
      '#title' => t("Select feature modules to display"),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 1,
    );
    $feature_select_options = array();
    foreach ($feature_infos['modules'] as $module) {
      $feature_select_options[$module['name']] = $module['name'];
    }
    $feature_select_default = !empty($form_state['values']['feature_select']) ? $form_state['values']['feature_select'] : $feature_select_options;
    $form['feature_select_fieldset']['feature_select'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Selected features'),
      '#default_value' => $feature_select_default,
      '#options' => $feature_select_options,
      
      '#ajax' => array(
        'callback' => 'features_overview_show_info_callback',
        'wrapper' => 'feature-overview',
        'effect' => 'fade',
      ),
    );
  }

  $form['feature_info_fieldset'] = array(
    '#title' => t("Feature overview"),
    // The prefix/suffix provide the div that we're replacing, named by
    // #ajax['wrapper'] above.
    '#prefix' => '<div id="feature-overview">',
    '#suffix' => '</div>',
    '#type' => 'fieldset',
    '#description' => t('Overview of the contents of all enabled feature modules'),
    '#weight' => 5,
  );
  
  $selected_modules = !empty($form_state['values']['feature_select']) ? $form_state['values']['feature_select'] : $feature_select_options;
  
  $form['feature_info_fieldset']['feature_overview'] = array(
    '#markup' => _features_overview_tables($feature_infos, $selected_modules),
    
  );
  return $form;
}

/**
 * Selects the piece of the form we want to use as replacement text and returns
 * it as a form (renderable array).
 *
 * @return renderable array (the textfields element)
 */
function features_overview_show_info_callback($form, $form_state) {
  return $form['feature_info_fieldset'];
}

/*
 * an overview of all enabled features as a table view.
 */
function _features_overview_tables($feature_infos, $selected_modules) {
  if (empty($feature_infos['modules'])) {
    return t('No Features were found. Please use the !create_link link to create
      a new Feature module, or upload an existing Feature to your modules directory.',
      array('!create_link' => l(t('Create Feature'), 'admin/structure/features/create')));
  }
  else {
    $render_items = '';

    // show a table with all module dependencies in matrix style
    $tablevars = array();
    $tablevars['attributes']['class'][] = 'feature-infos';
    $tablevars['attributes']['class'][] = 'feature-infos-matrix';
    $tablevars['header'][0]['data'] = '&nbsp;';
    foreach ($feature_infos['modules'] as $j => $value) {

      if ($selected_modules[$value['name']] && $selected_modules[$value['name']] == $value['name']) {

        // table header shows feature module names
        $tablevars['header'][$j+1]['data'] = '<div class="cell-data">' . l($value['name'], 'admin/structure/features/'. $value['machine_name'], array('attributes' => array('title' => t('Go to status page of @feature-name', array('@feature-name' => $value['name']))))) . '</div>';
        $tablevars['header'][$j+1]['data'] .= l('x', 'admin/structure/features/overview', array('attributes' => array('title' => t('Toggle display of @feature-name', array('@feature-name' => $value['name'])) , 'data-toggle-feature' => drupal_html_class($value['machine_name']), 'class' => array('toggle-inactive', 'column-toggle'))));
        $tablevars['header'][$j+1]['class'] = 'feature-module-name' . ' feature-' . drupal_html_class($value['machine_name']);
        if (!$value['status']) {
          $tablevars['header'][$j+1]['class'] .= ' feature-disabled';
        }
      }
    }
    reset($feature_infos['feature_dependencies']);
    
    foreach ($feature_infos['feature_dependencies'] as $k => $dependency) {
      
      $tablevars['rows'][$k][0]['data'] = $dependency;
      $tablevars['rows'][$k][0]['header'] = TRUE;
      if(in_array($dependency, $feature_infos['available_features'])) {
        $tablevars['rows'][$k][0]['class'] = 'feature-inner-dependency';
      }
      reset($feature_infos['modules']);
      foreach ($feature_infos['modules'] as $j => $value) {
        if ($selected_modules[$value['name']] && $selected_modules[$value['name']] == $value['name']) {
          if (!in_array($dependency, $value['dependencies'])) {
            $tablevars['rows'][$k][$j+1]['data'] = '<div class="cell-data">&nbsp;</div>';
            $tablevars['rows'][$k][$j+1]['class'] = 'feature-'. drupal_html_class($value['machine_name']);
          }
          else {
            $tablevars['rows'][$k][$j+1]['data'] = '<div class="cell-data"><strong>X</strong></div>';
            if(in_array($dependency, $feature_infos['available_features'])) {
              $tablevars['rows'][$k][$j+1]['class'] = 'feature-inner-dependency';
            }
            else {
              $tablevars['rows'][$k][$j+1]['class'] = 'dependency';
            }
            if (!$value['status']) {
              $tablevars['rows'][$k][$j+1]['class'] .= ' feature-disabled';
            }
            $tablevars['rows'][$k][$j+1]['class'] .= ' feature-' . drupal_html_class($value['machine_name']);
          }
        }
      }
    }
    if (!empty($tablevars['header'])) {
      $render_items .= '<div class="feature-infos-dependency">';
      $render_items .= '<h2>' . t('Module dependencies') . '</h2>';
      $render_items .= theme('table', $tablevars);
      $render_items .= '</div>';
    }
    

    // every type of feature gets rendered in a seperate table
    foreach ($feature_infos['feature_types'] as $type => $count) {
      $tablevars = array();
      $tablevars['attributes']['class'][] = 'feature-infos';
      $render_items .= '<div class="feature-infos-' . $type . '">';
      $render_items .= '<h2>' . t('Exported feature <em>') . $type . '</em></h2>';

      foreach ($feature_infos['modules'] as $j => $value) {
        if ($selected_modules[$value['name']] && $selected_modules[$value['name']] == $value['name']) {
          
          if (array_key_exists($type, $value['features'])) {
            // table header shows feature module names
            $tablevars['header'][$j]['data'] = '<div class="cell-data">' . l($value['name'], 'admin/structure/features/'. $value['machine_name'], array('attributes' => array('title' => t('Go to status page of @feature-name', array('@feature-name' => $value['name']))))) . '</div>';
            $tablevars['header'][$j]['data'] .= l('x', 'admin/structure/features/overview', array('attributes' => array('title' => t('Toggle display of @feature-name', array('@feature-name' => $value['name'])) , 'data-toggle-feature' => drupal_html_class($value['machine_name']), 'class' => array('toggle-inactive', 'column-toggle'))));
            $tablevars['header'][$j]['class'] = 'feature-module-name' . ' feature-' . drupal_html_class($value['machine_name']);
            if (!$value['status']) {
              $tablevars['header'][$j]['class'] .= ' feature-disabled';
            }
            // table body shows a feature module's exported features of this type in a column
            foreach ($value['features'][$type] as $k => $val) {
              $tablevars['rows'][$k][$j]['data'] = '<div class="cell-data">' . $val . '</div>';
              $tablevars['rows'][$k][$j]['class'] = 'exported-feature';

              if (!$value['status']) {
                $tablevars['rows'][$k][$j]['class'] .= ' feature-disabled';
              }
              $tablevars['rows'][$k][$j]['class'] .= ' feature-' . drupal_html_class($value['machine_name']);
            }
            // fill in empty cells in rows up to the maximum count of exported features of all listed feature modules
            if(++$k < $count) {
              for ($n = $k; $n < $count; $n++) {
                $tablevars['rows'][$n][$j]['data'] = '<div class="cell-data">&nbsp;</div>';
                $tablevars['rows'][$n][$j]['class'] = ' feature-' . drupal_html_class($value['machine_name']);
              }
            }
          }
        }
      }

      $render_items .= theme('table', $tablevars);
      $render_items .= '</div>';
    }
    
    return $render_items;
  }

}

function _features_overview_get_infos($features) {
  $i = 0;
  $feature_infos = array();
  $feature_infos['dependency_count_max'] = 0;
  $feature_infos['feature_types'] = array();
  $feature_infos['feature_dependencies'] = array();
  // collect infos needed for rendering the tables containing the infos on the
  
  foreach ($features as $name => $module) {
    $feature_infos['available_features'][] = $module->name;
    $feature_infos['modules'][$i]['machine_name'] = $module->name;
    $feature_infos['modules'][$i]['name'] = $module->info['name'];
    $feature_infos['modules'][$i]['status'] = $module->status;
    $feature_infos['modules'][$i]['dependencies'] = $module->info['dependencies'];
    if (count($feature_infos['modules'][$i]['dependencies']) > $feature_infos['dependency_count_max']) {
      $feature_infos['dependency_count_max'] = count($feature_infos['modules'][$i]['dependencies']);
    }
    foreach($feature_infos['modules'][$i]['dependencies'] as $dependency) {
      if (!in_array($dependency, $feature_infos['feature_dependencies'])) {
        $feature_infos['feature_dependencies'][] = $dependency;
      }
    }
    $feature_infos['modules'][$i]['features'] = $module->info['features'];
    foreach ($feature_infos['modules'][$i]['features'] as $feature_type => $exported_features) {
      if (!array_key_exists($feature_type, $feature_infos['feature_types'])) {
        $feature_infos['feature_types'][$feature_type] = count($exported_features);
      }
      elseif ($feature_infos['feature_types'][$feature_type] < count($exported_features)) {
        $feature_infos['feature_types'][$feature_type] = count($exported_features);
      }
    }
    $i++;
  }
  sort($feature_infos['feature_dependencies']);
  return $feature_infos;
}
